//
//  GameScene.swift
//  NewShooterGame
//
//  Created by Dan on 25/11/2014.
//  Copyright (c) 2014 Dan. All rights reserved.
// 


import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate{

  

    
    var shuttle:SKSpriteNode = SKSpriteNode()
    var LYieldInterval:NSTimeInterval = NSTimeInterval()
    var LUTimerInterval:NSTimeInterval = NSTimeInterval()
    var enemysDestroyed:Int = 0
    let enemyCat:UInt32 = 0x1 << 1
    let bulletCat:UInt32 = 0x1 << 0
    
   
  
    override func didMoveToView(view: SKView) {
       
        self.backgroundColor = SKColor.blackColor()
        shuttle = SKSpriteNode(imageNamed: "Ship")
        shuttle.position = CGPointMake(self.frame.size.width/2, shuttle.frame.size.height/2 + 22)
        self.addChild(shuttle)
        //gives 0 gravity
        self.physicsWorld.gravity = CGVectorMake(0,0)
        self.physicsWorld.contactDelegate = self
        
        //labels
        score.fontColor = SKColor.whiteColor()
        score.position = CGPointMake(self.size.width / 2, self.size.height - 70)
        level.fontColor = SKColor.blueColor()
        level.position = CGPointMake(self.size.width / 2 + 320, self.size.height - 70)
        taunt.fontColor = SKColor.blueColor()
        taunt.position = CGPointMake(self.size.width / 2 - 320, self.size.height - 70)
        
        //self.addChild(pauseText)
        self.addChild(score)
        self.addChild(level)
        self.addChild(taunt)
        
    }
    
    var level:SKLabelNode = SKLabelNode(fontNamed: "Superclarendon-Bold")
    var taunt:SKLabelNode = SKLabelNode(fontNamed: "Superclarendon-Bold")
    
    func updateTimeSinceLastUpdate(timeSinceLastUpdate:CFTimeInterval){
        LYieldInterval += timeSinceLastUpdate
       //increased enemy drop rate
        if enemysDestroyed > 5{
             self.level.text = String("level - 2")
            level.fontColor = SKColor.redColor()
            if enemysDestroyed > 15{
                self.level.text = String("level - 3")
                level.fontColor = SKColor.blueColor()
                self.taunt.text = String("Faster?")
                taunt.fontColor = SKColor.redColor()
                if enemysDestroyed > 25{
                    self.level.text = String("level - 4")
                    level.fontColor = SKColor.redColor()
                    self.taunt.text = String("Hows that?")
                    taunt.fontColor = SKColor.blueColor()
                    if enemysDestroyed > 35{
                        self.level.text = String("level - 5")
                        level.fontColor = SKColor.purpleColor()
                        self.taunt.text = String("Brah, no way!")
                        taunt.fontColor = SKColor.purpleColor()
                        if (LYieldInterval > 0.4){
                            LYieldInterval = 0
                            addEnemy()}
                    }else{
                        if (LYieldInterval > 0.7){
                            LYieldInterval = 0
                            addEnemy()}
                    }
                    if (LYieldInterval > 0.7){
                        LYieldInterval = 0
                        addEnemy()}
                }else{
                    if (LYieldInterval > 1.0){
                        LYieldInterval = 0
                        addEnemy()}
                }
                if (LYieldInterval > 1.0){
                    LYieldInterval = 0
                    addEnemy()}
            }else{
            if (LYieldInterval > 1.0){
                LYieldInterval = 0
                addEnemy()
            }
            }
        }else{
        if (LYieldInterval > 1.2){
            LYieldInterval = 0
            addEnemy()
            self.level.text = String("level - 1")        }
        }
       
        func stopAll(){
            removeAllActions()
            removeAllChildren()
            
        }
        
        
    }
    
    
        var score:SKLabelNode = SKLabelNode(fontNamed: "Superclarendon-Bold")
        var pauseText:SKLabelNode = SKLabelNode(fontNamed: "Superclarendon-Bold")
        var resumeText:SKLabelNode = SKLabelNode(fontNamed: "Superclarendon-Bold")
    func addEnemy(){
        var enemy:SKSpriteNode = SKSpriteNode(imageNamed: "enemy")
        enemy.physicsBody = SKPhysicsBody(rectangleOfSize: enemy.size)
        enemy.physicsBody?.dynamic = true
        
        enemy.physicsBody?.categoryBitMask = enemyCat
        enemy.physicsBody?.contactTestBitMask = bulletCat
        enemy.physicsBody?.collisionBitMask = 0
        
        let minX = enemy.size.width/2
        
        let maxX = self.frame.size.width - enemy.size.width/2
        let rangeX = maxX - minX
        let position:CGFloat = CGFloat(arc4random()) % CGFloat(rangeX) + CGFloat(minX)
        
        enemy.position = CGPointMake(position, self.frame.size.height+enemy.size.height)
        self.addChild(enemy)
                //movement
        
        let minDur = 3
        let maxDur = 5
        let rangeDur = maxDur - minDur
        let Dur = Int(arc4random()) % Int(rangeDur) + Int(minDur)
        
        
        
        var Action:NSMutableArray = NSMutableArray()
        Action.addObject(SKAction.moveTo(CGPointMake(position, -enemy.size.height), duration:NSTimeInterval(Dur)))
        Action.addObject(SKAction.runBlock({
           var transition:SKTransition = SKTransition.flipVerticalWithDuration(0.5)
            var gameoverScene:SKScene = GameOver(size: self.size, won: false)
            self.view?.presentScene(gameoverScene, transition: transition)
            
        }))
        
        Action.addObject(SKAction.removeFromParent())
        enemy.runAction(SKAction.sequence(Action))
        
    }
    

    
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            
            
           
            var location:CGPoint = touch.locationInNode(self)
            var bullet:SKSpriteNode = SKSpriteNode(imageNamed: "bullet")
            bullet.position = shuttle.position
            bullet.physicsBody = SKPhysicsBody(circleOfRadius: bullet.size.width/2)
            bullet.physicsBody?.dynamic = true
            bullet.physicsBody?.categoryBitMask = bulletCat
            bullet.physicsBody?.contactTestBitMask = enemyCat
            bullet.physicsBody?.collisionBitMask = 0
            bullet.physicsBody?.usesPreciseCollisionDetection = true
            
            var offset:CGPoint = vecMin(location, b: bullet.position)
            if (offset.y < 0){
                return
            }
            self.addChild(bullet)
            var direct:CGPoint = vecNorm(offset)
            var shootlen:CGPoint = vecTimes(direct, b:1000)
            var endDest:CGPoint = vecAdd(shootlen, b: bullet.position)
            let velocity = 568/1
            let moveDur:Float = Float(self.size.width) / Float(velocity)
            
            var actionArray:NSMutableArray = NSMutableArray()
            actionArray.addObject(SKAction.moveTo(endDest, duration: NSTimeInterval(moveDur)))
            actionArray.addObject(SKAction.removeFromParent())
            bullet.runAction(SKAction.sequence(actionArray))
            
            //pause section
          
            
           
            var pause1 = SKSpriteNode (imageNamed: "pause.png")
           
            // pause1.size = CGSizeMake(50, 50)
            pause1.position = CGPointMake(self.frame.size.width/2 - 350, self.frame.size.height/2 - 320  )
           
           addChild(pause1)
            
            if self.nodeAtPoint(location) == pause1
                {
                    if  self.view?.paused == true{
                        self.view?.paused = false
                        println("unpaused")
                    }else{
                      println("paused")
                self.view?.paused = true
              //  pause1.removeFromParent()
               
                    }
            }
            
    }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstCon:SKPhysicsBody
        var secondCon:SKPhysicsBody
        //if bullet is less than enemy, compare which body is which
        if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask){
            firstCon = contact.bodyA
            secondCon = contact.bodyB
            
        }else{
            firstCon = contact.bodyB
            secondCon = contact.bodyA
        }
        if((firstCon.categoryBitMask * bulletCat) != 0 && (secondCon.categoryBitMask & enemyCat) != 0){
        bulletCollision(firstCon.node as SKSpriteNode, enemy: secondCon.node as SKSpriteNode)
            
        }
    }
    
    func bulletCollision(bullet:SKSpriteNode, enemy:SKSpriteNode){
        println("Bullseye")
        
        bullet.removeFromParent()
        enemy.removeFromParent()
        
        enemysDestroyed++
        self.score.text = String(enemysDestroyed)
        if (enemysDestroyed >= 50){
            var transition:SKTransition = SKTransition.flipVerticalWithDuration(0.5)
            var gameoverScene:SKScene = GameOver(size: self.size, won: true)
            self.view?.presentScene(gameoverScene, transition: transition)
                   }
    }
    
   
    override func update(currentTime: CFTimeInterval) {
     var timeSinceLastUpdate = currentTime - LUTimerInterval
        LUTimerInterval = currentTime
        if (timeSinceLastUpdate  > 1){
            timeSinceLastUpdate = 1/60
            LUTimerInterval = currentTime
        }
        updateTimeSinceLastUpdate(timeSinceLastUpdate)
    }
    func vecAdd(a:CGPoint, b:CGPoint)->CGPoint{
        
        return CGPointMake(a.x + b.x, a.y + b.y)
    }
    func vecMin(a:CGPoint, b:CGPoint)->CGPoint{
        return CGPointMake(a.x - b.x, a.y - b.y)
        
}
    func vecTimes(a:CGPoint, b:CGFloat)->CGPoint{
        return CGPointMake(a.x * b, a.y * b)
    }

    func vecLen(a:CGPoint)->CGFloat{
        return CGFloat(sqrtf(CFloat(a.x)*CFloat(a.x)+CFloat(a.y)*CFloat(a.y)))
        
    }
    
    func vecNorm(a:CGPoint)->CGPoint{
        var length:CGFloat = vecLen(a)
        return CGPointMake(a.x / length, a.y / length)
    }
    
}