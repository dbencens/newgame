//
//  GameOver.swift
//  NewShooterGame
//
//  Created by Dan on 27/11/2014.
//  Copyright (c) 2014 Dan. All rights reserved.
//

import UIKit
import SpriteKit

class GameOver: SKScene {
    
   
     init(size: CGSize, won:Bool) {
    super.init(size: size)
    

   self.backgroundColor = SKColor.blackColor()
    var message:NSString = NSString()
    
        
        
        
        if (won){
            message = "Winner"
            
        }else{
            message = "Game Over"
        }
        var label:SKLabelNode = SKLabelNode(fontNamed: "Superclarendon-Bold")
    label.text = message
        label.fontColor = SKColor.whiteColor()
        label.position = CGPointMake(self.size.width/2, self.size.height/2)
self.addChild(label)
    self.runAction(SKAction.sequence([SKAction.waitForDuration(2.5),
        SKAction.runBlock({
            var transition:SKTransition = SKTransition.flipVerticalWithDuration(1.5)
            var scene:SKScene = GameScene(size: size)
            
            self.view?.presentScene(scene, transition: transition)
          
        })
        ]))
    }

     required init?(coder aDecoder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }

     }
    

