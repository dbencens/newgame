//
//  GameViewController.swift
//  NewShooterGame
//
//  Created by Dan on 25/11/2014.
//  Copyright (c) 2014 Dan. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

extension SKNode {
    class func unarchiveFromFile(file : NSString) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as GameScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}



class GameViewController: UIViewController {

    var backgroundmusicPlayer: AVAudioPlayer = AVAudioPlayer()
    
 //   func hitSound(){
        
    //    var hitSoundPlayer: AVAudioPlayer = AVAudioPlayer()
   //     var bgMusicURL:NSURL = NSBundle.mainBundle().URLForResource("hitSound", withExtension: "mp3")!
   //     hitSoundPlayer = AVAudioPlayer(contentsOfURL: bgMusicURL, error: nil)
   //     hitSoundPlayer.numberOfLoops = 1
    //    hitSoundPlayer.prepareToPlay()
    //    hitSoundPlayer.play()
   // }
    
   
    
    @IBAction func menuButton(sender: AnyObject) {
        backgroundmusicPlayer.stop()
    backgroundmusicPlayer.numberOfLoops = 0
     backgroundmusicPlayer.pause()
       GameScene().removeAllActions()
        GameScene().removeAllChildren()
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
    }

    
    override func viewWillLayoutSubviews() {
       if let scene = GameScene.unarchiveFromFile("GameScene") as? GameScene {
        
            var bgMusicURL:NSURL = NSBundle.mainBundle().URLForResource("backgroundmusic", withExtension: "mp3")!
        backgroundmusicPlayer = AVAudioPlayer(contentsOfURL: bgMusicURL, error: nil)
        backgroundmusicPlayer.numberOfLoops = -1
        backgroundmusicPlayer.prepareToPlay()
        backgroundmusicPlayer.play()
        
        
        
        var sKView:SKView = self.view as SKView
        sKView.showsFPS = false
        sKView.showsNodeCount = false
        scene.scaleMode = SKSceneScaleMode.Fill
        sKView.presentScene(scene)
        }
    }
    
    
    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
        } else {
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
